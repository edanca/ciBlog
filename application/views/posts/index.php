<h2><?= $title ?></h2>

<?php foreach ($posts as $post) : ?>

	<h3><?php echo $post['title']; ?></h3>

	<div class="row">
		<div class="col-md-3">
			<a href="<?php echo site_url('/posts/' . $post['slug']); ?>">
				<img class="post-thumb" src="<?php echo site_url(); ?>assets/images/posts/<?php echo $post['post_image']; ?>" >
			</a>
		</div>

		<div class="col-md-9">
			<small class="post-date">Posted on: <?php echo $post['created_at']; ?> in <strong><?php echo $post['name']; ?></strong> category, <?php $post['id']; ?></small><br>

			<?php echo word_limiter($post['body'], 50); ?>
			<br>

			<p><a class="btn btn-outline-secondary" href="<?php echo site_url('/posts/' . $post['slug']); ?>">Read more</a></p>			
		</div>
	</div>
	
	<br><br>

<?php endforeach; ?>
<div class="pagination-links">
	<?php echo $this->pagination->create_links(); ?>
</div>
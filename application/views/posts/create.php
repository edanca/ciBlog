<!-- Para usar las variables es importante usar ?= -->
<h2><?= $title; ?></h2>

<?php echo validation_errors(); ?>

<!-- Para crear una etiqueta mediante php es necesario agregarlo al helper en config/autoload.php -->
<?php echo form_open_multipart('posts/create'); ?>
<form>
  
  <div class="form-group">
    <label>Title</label>
    <input type="text" class="form-control" name="title" placeholder="Add Title">
  </div>
  
  <div class="form-group">
    <label>Body</label>
    <textarea id="editor" class="form-control" name="body" placeholder="Add Body" rows="15"></textarea>
  </div>

  <div class="form-group">
  	<label>Category</label>
  	<select name="category_id" class="form-control"> <!-- Para agregar seleccion multilinea se agrega el atributo "multiple" -->
  		<?php foreach($categories as $category): ?>
  			<option value="<?php echo $category['id'] ?>"><?php echo $category['name'] ?></option>}
  		<?php endforeach ?>
  	</select>
  </div>

  <div class="form-group">
    <label>Upload Image</label><br>
    <!-- En codeIgniter al subir un archivo tenemos que llamarlo 'userfile' para que funcione -->
    <input type="file" name="userfile" size="20">
  </div>

  <button type="submit" class="btn btn-default">Submit</button>
  <!-- <input type="submit" value="upload" class="btn btn-default"> -->
</form>
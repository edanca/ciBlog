<h2><?php echo $post['title']; ?></h2>

<small class="post-date">Posted on: <?php echo $post['created_at']; ?></small><br>

<img src="<?php echo site_url(); ?>assets/images/posts/<?php echo $post['post_image']; ?>">

<div class="post-body">
	<?php echo $post['body']; ?>
</div>

<hr>

<a href="<?php echo base_url() . 'posts/'?>" class="btn btn-warning float-left">Back</a>

<!-- Chekeamos si el post es del usuario logeado para poder borrar o editar -->
<?php if($this->session->userdata('user_id') == $post['user_id']) : ?>
	<a class="btn btn-secondary float-left" href="edit/<?php echo $post['slug'] ?>">Edit</a>
	<?php echo form_open('/posts/delete/' . $post['id']); ?>
		<input type="submit" value="Delete" class="btn btn-danger">
	</form>
<?php endif; ?>

<br>
<hr>

<h3>Comments</h3>
<?php if($comments) : ?>

	<?php foreach ($comments as $comment) : ?>
		<div class="card card-header">
			<h6><?php echo $comment['body']; ?> [by <strong><?php echo $comment['name']; ?></strong>]</h6>
		</div>
		<br>
	<?php endforeach; ?>

<?php else : ?>
	<p>No comments to display.</p>
<?php endif; ?>

<hr>

<!-- ############ -->
<!-- COMMENT AREA -->
<!-- ############ -->
<h3>Add Comment</h3>
<!-- Agregramos el formulario para validacion -->
<?php echo validation_errors(); ?>
<!-- To Comments Controller and function create -->
<?php echo form_open('/comments/create/' . $post['id']) ?>
	<div class="form-group">
		<label>Name</label>
		<input type="text" class="form-control" name="name" placeholder="Put here your name">
	</div>
	<div class="form-group">
		<label>Email</label>
		<input type="email" class="form-control" name="email" placeholder="Put here your email">
	</div>
	<div class="form-group">
		<label>Comment</label>
		<textarea name="body" class="form-control" placeholder="Enter your comment"></textarea>
	</div>
	<input type="hidden" name="slug" value="<?php echo $post['slug']; ?>">
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
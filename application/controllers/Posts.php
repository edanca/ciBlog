<?php

	class Posts extends CI_Controller {

		public function index($offset = 0) {
			// Pagination Condig
			$config['base_url'] = base_url() . 'posts/index/';
			$config['total_rows'] = $this->db->count_all('posts'); //in table
			$config['per_page'] = 2;
			$config['uri_segment'] = 3; // This is the segment after your base_url
			$config['num_links'] = 2; // numbers of links at each sides
			$config['attributes'] = array('class' => 'pagination-link');

			// Init pagination
			$this->pagination->initialize($config);

			// Display posts
			$data['title'] = 'Latest Posts';

			$data['posts'] = $this->post_model->get_posts(FALSE, $config['per_page'], $offset);
			// print_r($data['posts']);

			#view es el nombre de la funcion
			$this->load->view('templates/header');
			$this->load->view('posts/index', $data);
			$this->load->view('templates/footer');
		}


		public function view($slug = NULL) {
			$data['post'] = $this->post_model->get_posts($slug);
			$post_id = $data['post']['id'];
			$data['comments'] = $this->comment_model->get_comments($post_id);

			if (empty($data['post'])) {
				show_404();
			}

			$data['title'] = $data['post']['title'];

			$this->load->view('templates/header');
			$this->load->view('posts/view', $data);
			$this->load->view('templates/footer');
		}


		public function create() {
			// Check login
			if(!$this->session->userdata('logged_in')) {
				redirect('users/login');
			}

			$data['title'] = 'Create Post';

			// get_categories es un función creada en nuestro modelo post_model
			$data['categories'] = $this->post_model->get_categories();

			$this->form_validation->set_rules('title', 'Ttile', 'required');
			$this->form_validation->set_rules('body', 'Body', 'required');

			if($this->form_validation->run() === FALSE) {
				$this->load->view('templates/header');
				$this->load->view('posts/create', $data);
				$this->load->view('templates/footer');
			} else {
				// Upload image
				$config['upload_path'] = './assets/images/posts';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = '2048';
				$config['max_width'] = '2000';
				$config['max_height'] = '2000';

				$this->load->library('upload', $config);

				if(!$this->upload->do_upload() ) {
					$errors = array('error' => $this->upload->display_errors());
					$post_image = 'noimage.jpg';
				} else {
					$data = array('upload_data' => $this->upload->data());
					// En codeIgniter al subir un archivo tenemos que llamarlo 'userfile' para que funcione
					$post_image = $_FILES['userfile']['name'];
				}

				// pasamos la imagen al modelo para guardarlo en BD
				$this->post_model->create_post($post_image);

				// Set message
				$this->session->set_flashdata('post_created', 'Your post has been created');

				redirect('posts');
			}
		}

		
		public function delete($id) {
			// Check login
			if(!$this->session->userdata('logged_in')) {
				redirect('users/login');
			}

			$this->post_model->delete_post($id);

			// Set message
			$this->session->set_flashdata('post_deleted', 'Your post has been deleted');

			redirect('posts');
		}

		
		public function edit($slug) {
			// Check login
			if(!$this->session->userdata('logged_in')) {
				redirect('users/login');
			}

			$data['post'] = $this->post_model->get_posts($slug);
			$data['slug'] = $slug;

			// Check user
			if($this->session->userdata('user_id') != $this->post_model->get_posts($slug)['user_id']) {
				redirect('posts');
			}

			// get_categories es un función creada en nuestro modelo post_model
			$data['categories'] = $this->post_model->get_categories();

			if (empty($data['post'])) {
				show_404();
			}

			$data['title'] = 'Edit Post';

			$this->load->view('templates/header');
			$this->load->view('posts/edit', $data);
			$this->load->view('templates/footer');
		}


		public function update() {
			// Check login
			if(!$this->session->userdata('logged_in')) {
				redirect('users/login');
			}

			$this->post_model->update_post();

			// Set message
			$this->session->set_flashdata('post_updated', 'Your post has been updated');

			redirect('posts');
		}
	}

?>